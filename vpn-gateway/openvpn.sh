#!/usr/bin/env bash

set -o nounset

echo "ensuring tun device permissions"
mkdir -p /dev/net
[[ -c /dev/net/tun ]] || mknod -m 0666 /dev/net/tun c 10 200

echo "starting openvpn"
set -x

exec sg vpn -c "openvpn --config /etc/openvpn/*.conf --auth-user-pass /etc/openvpn/login.info --script-security 2 --data-ciphers AES-256-GCM:AES-128-GCM:AES-256-CBC --up /usr/bin/dante.sh"
