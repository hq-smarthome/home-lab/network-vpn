#!/bin/bash

set -e

[ -f /etc/openvpn/up.sh ] && /etc/openvpn/up.sh "$@"

exec sockd -f /etc/dante/sockd.conf -p /run/sockd.pid -N 10