job "network-vpn" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  constraint {
    attribute = "${attr.unique.hostname}"
    value = "proxima-b"
  }

  group "vpn-gateway" {
    count = 1

    network {
      port "socks" {
        to = "1080"
      }
    }

    task "vpn-gateway" {
      driver = "docker"

      vault {
        policies = ["vpn-gateway"]
      }

      config {
        image = "[[ .imageName ]]"

        ports = ["socks"]

        volumes = [
          "local/vpn-configs/se409.nordvpn.com.udp.ovpn:/etc/openvpn/se409.nordvpn.conf",
          "secrets/openvpn-login.info:/etc/openvpn/login.info"
        ]

        cap_add = [
          "NET_ADMIN"
        ]

        devices = [
          {
            host_path = "/dev/net/tun"
            container_path = "/dev/net/tun"
          }
        ]
      }

      template {
        data = <<EOH
{{ with secret "vpn-gateway/openvpn" }}{{ index .Data.data "username" }}
{{ index .Data.data "password" }}
{{ end }}
        EOH

        destination = "secrets/openvpn-login.info"
      }

      # Server selection will eventually be randomised from a large list
      artifact {
        source = "https://downloads.nordcdn.com/configs/files/ovpn_udp/servers/se409.nordvpn.com.udp.ovpn"
        destination = "local/vpn-configs"
      }
    }
  }
}