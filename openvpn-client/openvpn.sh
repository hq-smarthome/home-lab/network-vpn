#!/usr/bin/env bash

set -o nounset

echo "Setting up iptable rules"

docker_network="$(ip -o addr show dev eth0|awk '$3 == "inet" {print $4}')"

echo "Docker network: $docker_network"

iptables -F
iptables -X
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT

iptables -A INPUT -i eth0 -j ACCEPT

iptables -A FORWARD -i lo -j ACCEPT
iptables -A FORWARD -i eth0 -j ACCEPT

iptables -A OUTPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT

iptables -A OUTPUT -p udp -o eth0 --dport 53 -j ACCEPT
iptables -A INPUT -p udp -i eth0 --sport 53 -j ACCEPT

iptables -A OUTPUT -o tun+ -j ACCEPT
iptables -A OUTPUT -p udp -m udp --dport 1194 -j ACCEPT

iptables -A POSTROUTING -t nat -o tun+ -j MASQUERADE

echo "ensuring tun device permissions"
mkdir -p /dev/net
[[ -c /dev/net/tun ]] || mknod -m 0666 /dev/net/tun c 10 200

echo "starting openvpn"
set -x

exec sg vpn -c "openvpn --config /vpn/config.ovpn --auth-user-pass /vpn/login.info --script-security 2 --data-ciphers AES-256-GCM:AES-128-GCM:AES-256-CBC --auth-nocache --route-nopull"
