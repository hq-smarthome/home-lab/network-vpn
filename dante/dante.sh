#!/usr/bin/env bash

set -o nounset

set -x

exec sockd -f /etc/dante/sockd.conf -p /run/sockd.pid -N 10